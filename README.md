# ReactReduxBoilerplateApp
A Starter Kit to get started on a UI Project with Reactjs, Redux and Sass.


## Step 1
Run ```npm install ``` to install all the dependencies.


## Step 2
Run ```npm run start ``` to run it in the development environemnt.This will open the the StarterKit Landing page in http://localhost:8080/

Or

Run ```npm run build ``` to run it in the production environment.This will generate the JS and CSS files inside `dist/js` and `dist/css` folders.After that you can open the `index.html` page under `./dist` folder in your browser.


postgres : 

create the DataBase name : poc




CREATE TABLE public.account
(
    user_id integer NOT NULL DEFAULT nextval('account_user_id_seq'::regclass),
    username character varying(50) COLLATE pg_catalog."default" NOT NULL,
    password character varying(50) COLLATE pg_catalog."default" NOT NULL,
    email character varying(355) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(100) COLLATE pg_catalog."default",
    last_name character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT account_pkey PRIMARY KEY (user_id),
    CONSTRAINT account_email_key UNIQUE (email),
    CONSTRAINT account_username_key UNIQUE (username)
)