import React, { Component } from "react";



class Dashboard extends React.Component {

    render() {


        return (
            <div>
                <div className="heading1">
                    <h1>IHIES</h1>
                    <h4 className="padd">IHI Simulator Battery Lishen Overview</h4>
                    <table>
                        <tr className="bol">
                            <th className="head">Company</th>
                            <th className="head">Product</th>
                            <th className="head">Status</th>
                            <th className="head">Alarmed</th>
                            <th className="head">Communication Status</th>
                        </tr>
                        <tr>
                            <td className="hea">Lishen</td>
                            <td className="head">S4000K_LP27148134 Li-Ion</td>
                            <td className="head">SHUTDOWN</td>
                            <td className="head">NOFAULT</td>
                            <td className="head">NOT CONNECTED</td>
                        </tr>
                    </table>
                </div>
            </div>
        );
    }
}
export default Dashboard;