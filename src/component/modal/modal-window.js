import React from 'react';
import { connect } from 'react-redux';
import '@trendmicro/react-modal/dist/react-modal.css';
import Modal from '@trendmicro/react-modal';
import { Button } from '@trendmicro/react-buttons';
import { Field, reduxForm, reset } from 'redux-form';
import Textbox from '../input-component/text-box';

class ModelWindow extends React.Component {

    render() {
        let { title, openModel, saveData, recordId } = this.props;

        console.log("title", title);
        return (<Modal size={"sm"} onClose={() => openModel(false)}>
            <Modal.Header>
                <Modal.Title>
                    Register
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    [
                        <Field name="firstName" type="input"
                            component={Textbox} label="First Name"
                        />,
                        <Field name="lastName" type="input"
                            component={Textbox} label="Last Name"
                        />,
                        <Field name="email" type="input"
                            component={Textbox} label="Email"
                        />,
                        <Field name="userName" type="input"
                            component={Textbox} label="User Name"
                        />,

                        <Field name="password" type="password"
                            component={Textbox} label="password"
                        />
                    ]
                }
            </Modal.Body>
            <Modal.Footer>
                <Button
                    btnStyle="primary"
                    onClick={() => saveData(recordId)}
                >
                    <span>Save</span>

                </Button>
                <Button
                    btnStyle="default"
                    onClick={() => openModel(false)}
                >
                    Close
            </Button>
            </Modal.Footer>
        </Modal >);
    }


}

const modelForm = reduxForm({
    form: "modelForm",
    enableReinitialize: true
})(ModelWindow);



export default (modelForm);