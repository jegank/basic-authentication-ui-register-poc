import React from 'react';
import Header from './header';
import Footer from './footer';
import "../../css/Dashboard.css";
import '../../css/index.css';
import '../../css/App.css';
import '../../css/Alaram.css';
import '../../css/Information.css';
import '../../css/Register.css';
import '../../css/bootstrap.css';


class MainView extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <main className="site-main">
                    {this.props.children}
                </main>
                <Footer />
            </div>
        );
    }
};

export default (MainView);
