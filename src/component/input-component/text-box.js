import React from 'react';

const required = value => value ? undefined : 'Required'

export default ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className="form-group">
        <label >{label}</label>
        <input {...input} className="form-control" type={type} />
        <div>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
    </div>
)
