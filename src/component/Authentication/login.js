import React, { Component } from "react";
import { connect } from 'react-redux';
import { formUpload, registerFormUpload } from '../../redux/actions/login';
import ModalWindow from '../modal/modal-window';
import { Field, reduxForm, change } from 'redux-form';
import Textbox from '../input-component/text-box';

class Login extends React.Component {


    constructor(props) {
        super(props);
        this.openModal = this.openModal.bind(this);
        this.state = { openModal: false, errorMessage: [] };
        this.saveData = this.saveData.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.errorMessage = this.errorMessage.bind(this);
    }

    openModal(value) {
        this.setState({ openModal: value, errorMessage: [] });
    }

    errorMessage(error) {
        this.setState({ errorMessage: error });
    }

    saveData() {
        this.props.register().then((res) => {
            this.openModal(false);
        }).catch(error => {
            this.errorMessage(error);
        });
    }

    onFormSubmit() {
        this.props.uploadFormData().then((res) => {
            this.props.history.push("/welcome");
        }).catch(error => {
            this.errorMessage(error);
        });
    }


    render() {
        let { handleSubmit } = this.props;
        return (
            <div>

                {

                    this.state.errorMessage && this.state.errorMessage.map(err => {
                        return <div style={{ color: "red" }}>{err}</div>;
                    })
                }




                <div >
                    {

                        this.state.openModal && <ModalWindow openModel={this.openModal} saveData={this.saveData} />
                    }
                    <section className="login_box_area section-margin">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-3">
                                </div>
                                <div className="col-lg-6">
                                    <div className="login_form_inner">
                                        <h3>Log in to enter</h3>
                                        <form className="row login_form" onSubmit={handleSubmit(this.onFormSubmit)}>

                                            <div className="col-md-12 form-group">
                                                <Field name="userName" type="input"
                                                    component={Textbox} label="User Name"
                                                />
                                            </div>
                                            <div className="col-md-12 form-group">
                                                <Field name="password" type="password"
                                                    component={Textbox} label="Password"
                                                />
                                            </div>

                                            <div className="col-md-12 form-group">
                                                <button type="submit" value="submit" className="button button-login w-100">Log
                                                    In
                                        </button>

                                            </div>
                                        </form>
                                        <div className="col-md-12 form-group">
                                            <a href="#" onClick={() =>
                                                this.openModal(true)
                                            }>If you are not Registered please sing up</a></div>
                                    </div>
                                </div>
                                <div className="col-lg-3">
                                </div>
                            </div>
                        </div>
                    </section>
                </div >
            </div >
        );
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        uploadFormData: () => dispatch(formUpload()),
        register: () => dispatch(registerFormUpload())
    }
}

const mapStateToProps = state => {



    let errorMessage = state.login && state.login.error;
    console.log("state", errorMessage);
    return {
        errorMessage
    };
}

const loginForm = reduxForm({
    form: 'login',
    enableReinitialize: true
})(Login);

export default connect(mapStateToProps, mapDispatchToProps)(loginForm);
