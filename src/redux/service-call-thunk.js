import axios from "axios";

export const serviceCallThunk = (config, success, fail) => {

    return dispatch => new Promise((resolve, reject) => {
        axios(config).then((response) => {
            console.log("response--->", response);
            resolve(success ? dispatch(success(response.data)) : response.data);
        }).catch(error => {
            let message = error.response.data;
            reject(fail ? dispatch(fail(message)) : message);
        });
    });
}
