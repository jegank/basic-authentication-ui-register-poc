import { applyMiddleware, createStore } from 'redux';
//import createHistory from 'history/createBrowserHistory';


import reducer from './combine-reducer';
import thunk from 'redux-thunk';

const createHistory = require("history").createBrowserHistory;

const history = createHistory();

const getMiddleware = () => {
    return applyMiddleware(thunk);
};

const store = createStore(reducer, getMiddleware());

export {
    store,
    history
}