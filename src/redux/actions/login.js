import { serviceCallThunk } from "../service-call-thunk";
export const service_call_success = 'SERVICE_CALL_SUCCESS';
export const service_call_failure = 'SERVICE_CALL_FAILURE';

export const service_call_success_with_single_item = 'SERVICE_CALL_SUCCESS_WITH_SINGLE_ITEM';


export const serviceCallSuccess = payLoad => {
    return {
        type: service_call_success,
        payLoad
    }
}

export const serviceCallSuccessWithSingleItem = payLoad => {
    return {
        type: service_call_success_with_single_item,
        payLoad
    }
}

export const serviceCallFailure = payLoad => {
    return {
        type: service_call_failure,
        payLoad
    }
}

export const formUpload = () => {
    return (dispatch, getState) => {
        const formData = getState().form.login.values;
        const config = {
            headers: {
                'content-type': 'application/json',
            },
            data: { ...formData },
            url: `~^service, /auth~^`,

            method: "POST"
        }
        return dispatch(serviceCallThunk(config, serviceCallSuccess));
    }
}

export const registerFormUpload = () => {
    return (dispatch, getState) => {
        const formData = getState().form.modelForm.values;
        const config = {
            headers: {
                'content-type': 'application/json',
            },
            data: { ...formData },
            url: `~^service, /register~^`,

            method: "POST"
        }
        return dispatch(serviceCallThunk(config, serviceCallSuccess));
    }
}