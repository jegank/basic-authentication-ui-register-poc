import { service_call_success, service_call_failure, service_call_success_with_single_item } from '../actions/bug-data';







export default (state = [], action) => {
    switch (action.type) {
        case service_call_success:
            return {
                bugs: action.payLoad
            }

        case service_call_success_with_single_item:
            return {
                ...state,
                editData: action.payLoad
            }
        default:
            return state;
    }
};
