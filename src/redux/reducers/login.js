import { service_call_success, service_call_failure } from '../actions/login';

export default (state = {}, action) => {
    switch (action.type) {
        case service_call_failure:
            return {
                error: action.payLoad
            }
        default:
            return state;
    }
};