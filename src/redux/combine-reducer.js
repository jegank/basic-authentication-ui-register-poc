import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import login from './reducers/login';
import register from './reducers/register';



export default combineReducers({
    form: formReducer,
    login,
    register
});