import React from 'react';
import { Route, Switch } from 'react-router-dom';
import MainView from './component/main-view/main-view';
import loginForm from './component/Authentication/login';
import welcome from './component/welcome/welcome';


class App extends React.Component {

    render() {
        return (
            <MainView header={""} footer={""}>
                <Switch>
                    <Route exact path="/" component={loginForm} />
                    <Route exact path="/welcome" component={welcome} />
                </Switch>
            </MainView>
        );
    }
}

export default App;
